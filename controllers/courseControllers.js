const Course = require('../models/Course');

// Create a new course
/*
	Steps:
	1. Create a new Course object
	2. Save to the database
	3. Error handling

*/

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	// CreateNewObject
	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	// Saves the created objet to our database
	return newCourse.save().then((course, error) => {
		if(error) {
			return false;
		} else {
			// Course creation successful
			return true;
		}
	})
};

// Retrieving All courses

module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
};


// Retrieve get All ACtive
module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then( result => {
		return result;
	})
};

// Get a course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
};

// Update a course
/*
	Steps:
	1. Create a variable which will condtain the information retrieved from the request body.
	2. Find and update course using the course ID.
*/
module.exports.updateCourse = (courseId, reqBody) => {
	// Specify the properties of the document to be updated.
	let updatedCourse = {
		name: reqBody.name,
		decription: reqBody.description, 
		price: reqBody.price
	};

	// findByIdAndUpdate(id), updatesToBeApplied
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course,err) => {
		if(err) {	
			return false;
		} else {
			// Course updated successfullu
			return true;
		}
	})
};

// Archive a course
module.exports.archiveCourse = (courseId, res) => {
	let newStatus = {
		isActive: false
	}
	// findByIdAndUpdate(id), updatesToBeApplied
	return Course.findByIdAndUpdate(courseId, newStatus).then((result,err) => {
		if(err) {	
			return false;
		} else {
			// Course updated successfullu
			return {
				message: "Course successfully archived."
			};
		}
	})
};