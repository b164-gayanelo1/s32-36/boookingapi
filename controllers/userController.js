const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt'); /*Password encryption*/
const auth = require('../auth');


// Check if the email already exists
/*
	Business logic:
	1. Use mongoose "find" method to find duplicate emails.
	2. Use the "then" method to send a response back to the client.
*/
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			// No duplicate email found
			return false;
		}
	})
}

// User Registration
/*
	Business Logic
	1. Create a new User Object.
	2. Make sure that the password is encrypted.
	3. Save the new User to the database.
*/
module.exports.registerUser = (reqBody) => {

	// Creates a New User Object
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		// 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in the order to encrypt the password.
		password: bcrypt.hashSync(reqBody.password, 10) 

	})

	return newUser.save().then((user,error) => {
		// User registration failed.
		if(error){
			return false;
		} else {
			// User registration success.
			return true;
		}
	})
}


// User Authenticaiton(login)
/*
	Busines logic
	1. Check the database if the user email exists.
	2. Compare the password provided in the login form with the password stored in the database.
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not.
*/

module.exports.loginUser = (reqBody) => {
	
	// findOne it will returns the first record in the collection that matches the search criteria.
	return User.findOne({email: reqBody.email}).then(result => {
		// If the user does not exist
		if(result == null){
			return false;
		} else {
			// User exists

			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the databaseand returns "true" or "false".
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			// If the password matched
			if(isPasswordCorrect) {
				// Generate an access token
				return { accessToken : auth.createAccessToken(result.toObject()) }
			} else {
				// Password does not match
				return false;
			}
		}
	})
}


// Get Profile by ID
/*
	Business Logic:
	    a. Find the document in the database using the user's ID
        b. Reassign the password of the returned document to an empty string
        c. Return the result back to the frontend
*/
module.exports.getProfile = (profileId) => {
	return User.findById(profileId).then((result, err) => {
		if(err) {
			console.log(err);
			return error;
		} else {

			result.password = "";
			return result;

/*			return { 
				
				_id: result._id,
       			firstName: result.firstName,
       		 	lastName: result.lastName,
				mobileNo: result.mobileNo,
       			email: result.email,
      		  	password: "",
        		isAdmin: result.isAdmin,
        		enrollments: result.enrollments,
        		__v: result._v
			}*/
			
		}
	})
}

/*
// Post Profile by ID
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody).then((result, err) => {
		if(err) {
			console.log(err)
			return error;
		} else {

			result.password = "";
			return result;


			return { 
				
				_id: result._id,
       			firstName: result.firstName,
       		 	lastName: result.lastName,
				mobileNo: result.mobileNo,
       			email: result.email,
      		  	password: "",
        		isAdmin: result.isAdmin,
        		enrollments: result.enrollments,
        		__v: result._v

			}
			
		}
	})
}

*/

// Enroll a user
/*
	Steps:
	1. FInd the document in the database using the user's ID.
	2. Add the course ID to the user's enrollment array using the PUSH method.
	3. Add the userId to the courses enrollees arrays.
	4. Save the document.
	5. If both courseId and userId are successfully saved to the database, return true to the client.
*/

// Async and await - allow the processes to wait for each other.

module.exports.enroll = async (data) => {
	//Add the courseId to the enrollments array of the user


	let isUserUpdated = await User.findById(data.userId).then( user => {
		//push the course Id to enrollments property

			// Personal goal : Included validation.


		user.enrollments.push({ courseId: data.courseId});

		//save
		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true
			}
		})
	});


	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//add the userId in the course's database(enrollees)
		
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	});


	//Validation
	if(isUserUpdated && isCourseUpdated){
		//user enrollment successful
		return true;
	}else {
		//user enrollment failed
		return false;
	}

};
